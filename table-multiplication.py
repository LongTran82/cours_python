'''Réviser les tables de multiplication
'''


import random
from time import time

def table(x) -> int:
    # Les temps de réponse à chaque question seront ajoutés dans une liste
    liste_temps = []
    temps = 0
    # identifier le temps de réponse le plus long
    maximum = 0
    # nombre de bonnes réponses
    bon_rep = 0
    
    while True:
        for i in range(10):       
            try:
                ## t = [] #liste des nombres tirés aléatoirement
                # on initialise un nombre aléatoire
                y = random.randint(1,10)
                # t.append(y)
                ## while y not in t: --> ne fonctionne pas ?!
                print("Combien font", x, "X", y,"? ")
                # debut du temps, marqué par la question
                debut = time()
                # la bonne réponse espérée
                resultat = x * y
                reponse = int(input("ta réponse : "))
                # marque la fin du temps de réponse
                fin = time()
                # durée de réponse
                temps = fin - debut
                liste_temps.append(temps)
                maximum = max(liste_temps)

                # affiche si résultat est correct ou non
                if reponse == resultat:
                    print("réponse correcte")
                    # on incrémente les bonnes réponses
                    bon_rep += 1
                    i+= 1
                    
                else:
                    print("réponse incorrecte")
                    i+= 1

            except ValueError:
                reponse = input('rentre un nombre ')
            
        moyenne = sum(liste_temps)/len(liste_temps)
        print("le temps de réponse le plus long est ", round(maximum, 2), "seconde")
        print("le temps de réponse moyen est de ", round(moyenne, 2), "secondes")

        # mot d'appréciation selon le score obtenu
        if bon_rep == 10:
            print("Excelent")
        elif bon_rep == 9:
            print("Très bien")
        elif 8 <= bon_rep >= 7:
            print("Bien")
        elif 4 <= bon_rep <= 6:
            print('Moyen')
        elif 1 <= bon_rep <= 3:
            print('Il faut retravailler cette table')
        else:
            print("Est-ce que tu l'as fait exprés?")

        continuer = input("Veux tu continuer? o/n ")
        continuer = continuer.lower()

        # continuer ou pas de réviser   
        while continuer != 'n' and continuer != 'o':
            continuer = input('tape "o" pour continuer et "n" pour terminer ')
        else:
            if continuer == 'n':
                print('fin des révisions')
                return False

            else:
                continuer =='o'
                x = input("Choix d'une nouvelle table : ")
                return True

table(1)
