from labo import *
from menu import *

def gestion_labo():

    def cmd_enregistrer_arrivee():
        print("--Vous souhaitez enregistrer une arrivée--")
        try:
            personne = input("Entrez le nom de la personne : ")
            bureau = input("Quel est son bureau :")
            enregistrer_arrivee(mon_labo, personne, bureau)
        except PresentException:
            print("La personne fait déjà partie du laboratoire")

    def cmd_enregistrer_depart():
        print("--Vous souhaitez enregistrer un départ--")
        try:
            personne = input("Entrez le nom de la personne : ")
            enregistrer_depart(mon_labo, personne)
        except AbsentException:
            print("La personne ne fait pas partie du laboratoire")
         
    def cmd_modifier_bureau():
        print("--Affecter un nouveau bureau à une personne--")
        try:
            personne = input("Nom de la personne : ")
            new_bureau = input("Son nouveau bureau : ")
            modifier_bureau(mon_labo, personne, new_bureau)
        except AbsentException:
            print("La personne ne fait pas partie du laboratoire")

    def cmd_modifier_nom():
        print("--Vous voulez modifier le nom d'une personne--")
        try:
            personne = input("Son nom actuel : ")
            new_personne = input("Son nouveau nom : ")
            modfier_nom(mon_labo,personne, new_personne)
        except AbsentException:
            print("La personne ne fait pas partie du laboratoire")

    def cmd_personne_labo():
        print("--Recherche d'une personne--")
        personne = input("Nom de la personne recherchée: ")
        personne_labo(mon_labo, personne)

    def cmd_bureau_personne():
        print("--Rechercher le bureau d'une personne--")
        personne = input("Nom de la personne : ")
        bureau_personne(mon_labo, personne)

    def cmd_listing():
        print("--Afficher le listing du laboratoire--")
        try:
            listing(mon_labo)
        except LaboException:
            print("Il n'y pas personne dans le laboratoire")

    #def cmd_occupation_bureau():
    #    print("--Afficher l'occupation des bureaux--")
    #   occupation_bureau(mon_labo)
    
    def cmd_afficher_html():
        print("--Afficher l'occupation des bureaux")
        afficher_html(mon_labo)

    def cmd_importer_csv():
        fichier = input("Quel fichier souhaitez vous importer ? ")
        importer_csv(labo, fichier)

    def cmd_sauvegarder():
        fichier = input("Sauvez sous : ")
        sauvegarder(labo, fichier)



    mon_labo = labo()

    menu_labo = menu()
    ajouter_menu(menu_labo, "Enregistrer une arrivée", cmd_enregistrer_arrivee)
    ajouter_menu(menu_labo, "Enregistrer un départ", cmd_enregistrer_depart)
    ajouter_menu(menu_labo, "Modifier le bureau d'une personne", cmd_modifier_bureau)
    ajouter_menu(menu_labo, "Modifier le nom d'une personne", cmd_modifier_nom)
    ajouter_menu(menu_labo, "Chercher une personne dans le laboratoire", cmd_personne_labo)
    ajouter_menu(menu_labo, "Obtenir le bureau d'une personne", cmd_bureau_personne)
    ajouter_menu(menu_labo, "Listing de tous les membres et leurs affectations dans le laboratoire", cmd_listing)
    ajouter_menu(menu_labo, "Afficher les bureaux et leurs occupants dans un fichier HTML", cmd_afficher_html)
    ajouter_menu(menu_labo, "Importer un fichier csv", cmd_importer_csv)
    ajouter_menu(menu_labo, "Sauvegarder les données", cmd_sauvegarder)
    
    gerer_menu(menu_labo)

if __name__ == "__main__":
    gestion_labo()