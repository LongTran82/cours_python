""" [intutilué (#est le numero de la commande), commande (#l'action)] """

def menu():
    return []
    
def ajouter_menu(m, intitule, cmd):
    m.append((intitule, cmd))

def _afficher_menu(m):
    print('\n')
    for num, entree in enumerate(m,1):
        intitule,_ = entree
        print(num, intitule)
    print(0, 'Quitter')

def _input_choix(m):
    choix = input()
    taille_max = len(m)
    while choix.isdigit() == False or int(choix) < 0 or int(choix) > taille_max:
        choix = input("Merci de rentrer une option valide entre 0 et {} \n".format(taille_max))
    return int(choix)
    
    
def _traiter_choix(m, choix):
    if choix != 0 :
        m[choix-1] [1]()
    else:
        exit()

def gerer_menu(m):
    while True:
        _afficher_menu(m)
        choix = _input_choix(m)
        _traiter_choix(m, choix)


if __name__ == "__main__":
    menu()