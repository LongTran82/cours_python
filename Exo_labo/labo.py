import csv
import json

""" gestion de labo """

class LaboException(Exception):
    #Généralise les exceptions du laboratoire.
    pass


class AbsentException(LaboException):
    pass


class PresentException(LaboException):
    pass

def labo():
    return {}
    #test avec des données saisies
    #return{"pierre":"F300", "liam" : "A56", "Long":"F100", "Roger" : "F300", "JP": "F100", "FR" : "F300", "AZ": "F200", 'GT': 'F200', 'PI' : 'F300'}

def taille(labo):
    taille = len(labo)
    return taille

def enregistrer_arrivee(labo, personne, bureau):
    if personne in labo:
        raise PresentException
    else:
        labo[personne] = bureau

def enregistrer_depart(labo, personne):
    if personne not in labo:
        raise AbsentException
    else:
        del(labo[personne])

def modifier_bureau(labo, personne, new_bureau):
    if personne not in labo:
        raise AbsentException
    labo[personne] = new_bureau

def modfier_nom(labo, personne, new_personne):
    if personne not in labo:
        raise AbsentException
    labo[new_personne] = labo.pop(personne)

def personne_labo(labo, personne):
    if personne in labo:
        print(personne + " fait bien parti(e) du laboratoire")
    else:
        print(personne + " ne fait pas parti(e) du laboratoire")
    
def bureau_personne(labo, personne):
    if personne in labo:
        print(personne + " est dans le bureau " + labo[personne])
    else:
        print(personne + "ne fait pas parti(e) du laboratoire")

def listing(labo):
    if len(labo) == 0:
        raise LaboException("listing vide")
    else:
        for key, value in labo.items():
            print(key, "->", value)
    
def afficher_html(labo):
    labo_tri = {}
    for personne, bureau in labo.items():
        labo_tri.setdefault(bureau, []).append(personne)

    debut = '''
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Exercice Python Labo">
    <title>Bureaux</title>
</head>

<body>
    <h1>Liste des bureaux et personnels dans le laboratoire :</h1>
'''
    fin = '''
        </body>
        </html>
        '''

    with open('labo.html','w') as f:
        f.write(debut)
        for bureau, personne in sorted(labo_tri.items()):
            f.write('<h2>{}</h2>'.format(bureau))
            for noms in sorted(personne):
                f.write('<p>- {}</p>'.format(noms))
        f.write(fin)
    for bureau, personne in sorted(labo_tri.items()):
        print('\n {}'.format(bureau))
        for noms in sorted(personne):
            print('- {}'.format(noms))
    print("\n une liste a été générée dans une page html")

def importer_csv(labo, fichier):
    new_list_pers_bur = {}
    with open (fichier, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row['Personne'] in labo.keys():
                if labo[row['Personne']] != row['Bureau']:
                    new_list_pers_bur[row['Personne']] = [labo[row['Personne']], row['Bureau']]
            labo[row['Personne']] = row ['Bureau']
                
    return new_list_pers_bur

def sauvegarder(labo, fichier):
    with open(fichier+'.json', 'w') as save:
        json.dump(labo, save)